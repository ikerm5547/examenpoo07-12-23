/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenordinariopoo.pkg07.pkg12.pkg23;

/**
 *
 * @author Iker Martinez
 */
public class Recibo {
    private int numRecibo;
    private String Nombre;
    private int Puesto;
    private int Nivel;
    private int diasTrabajados;

    public Recibo(){
    this.numRecibo=23;
    this.Nombre="Jose Lopez";
    this.Puesto=2;
    this.Nivel=1;
    this.diasTrabajados=10;
    }
    
    public Recibo(int numRecibo, String Nombre, int Puesto, int Nivel, int diasTrabajados) {
        this.numRecibo = numRecibo;
        this.Nombre = Nombre;
        this.Puesto = Puesto;
        this.Nivel = Nivel;
        this.diasTrabajados = diasTrabajados;
    }
    
    public Recibo(Recibo otro){
        this.numRecibo = otro.numRecibo;
        this.Nombre = otro.Nombre;
        this.Puesto = otro.Puesto;
        this.Nivel = otro.Nivel;
        this.diasTrabajados = otro.diasTrabajados;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getPuesto() {
        return Puesto;
    }

    public void setPuesto(int Puesto) {
        this.Puesto = Puesto;
    }

    public int getNivel() {
        return Nivel;
    }

    public void setNivel(int Nivel) {
        this.Nivel = Nivel;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    
    public float Calcularpago(){
    float pago = 0.0f;
    if(this.Puesto == 1){pago = this.diasTrabajados * 100.00f;}
    if(this.Puesto == 2){pago = this.diasTrabajados * 200.00f;}
    if(this.Puesto == 3){pago = this.diasTrabajados * 300.00f;}
    return pago;
    }
    
    public float Impuestos(){
    float impuesto = 0.0f;
    float pago = this.Calcularpago();
    if(this.Nivel == 1){impuesto = pago * .05f;}
    if(this.Nivel == 2){impuesto = pago * .03f;}
    return impuesto;
    }
    
    public float total(){
    float total;
    total = this.Calcularpago() - this.Impuestos();
    return total;
    }
}
